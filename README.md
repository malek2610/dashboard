# Getting Started with React App

## Setting-up the Application
 1. Create new application within the same folder using the following command: 
    
    ```
    npx create-react-app ./
    ```

2. Delete the `src` folder and re-create an empty one
3. Create `index.js` file within the `src` folder
   
   ```JS
    import React from 'react';
    import ReactDOM from 'react-dom';

    import App from './App';

    ReactDOM.render(<App />, document.getElementById('root'));
    ```

4. Create `App.js` file within the `src` folder

    ```JS
    import React from 'react'

    const App = () => {
        return (
            <div>App</div>
        )
    }

    export default App
    ```
    Note: If the ES7+ extension is installed on VS Code, you can just use `rafce` to generate previous code

5. Install the needed dependencies for the project by typing the following command on the terminal:

    ```
    npm install --legacy-peer-deps
    ```

6. Finally you can start the application by running the following command:

    ```
    npm start
    ```

## VS Code Extensions
- [ES7+ React/Redux/React-Native snippets](https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets)
- [Tailwind CSS IntelliSense](https://marketplace.visualstudio.com/items?itemName=bradlc.vscode-tailwindcss)